package com.app.procurementdata;

import com.app.procurementdata.model.procurement;
import com.app.spring.repo.CustomersRepository;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProcurementDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcurementDataApplication.class, args);
	}
        
        @Bean
        public CommandLineRunner testCustomerRepository(CustomersRepository repo){
        return a -> {
            List<procurement> list = repo.findAll();
            System.out.format("%-15s $-40s $-30s $-20s \n","IdPR","Deskripsi","TanggalPR","RequesterPR","TotalPR");
                System.out.println("------------------------------------------------------------");
            list.forEach(c-> {
            System.out.format("%-15s $-40s $-30s $-20s  \n",c.getIdPR(),c.getDeskripsi(),c.getTanggalPR(),c.getRequesterPR(),c.getTotalPR());
            });
        };

    }
}