/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.procurementdata.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author user
 */
@Entity
@Table(name = "procurement")
public class procurement {
    @Id
    @Column (name = "IdPR")
    private String IdPR;
    
    @Column (name = "Deskripsi")
    private String Deskripsi;
    
    @Column (name = "TanggalPR")
    private String TanggalPR;
    
    @Column (name = "RequesterPR ")
    private String RequesterPR;
    
    @Column (name = "TotalPR")
    private String TotalPR;
    
    public procurement() {
    }

    public procurement(String IdPR, String Deskripsi, String TanggalPR, String RequesterPR, String TotalPR) {
        this.IdPR = IdPR;
        this.Deskripsi = Deskripsi;
        this.TanggalPR = TanggalPR;
        this.RequesterPR = RequesterPR;
        this.TotalPR = TotalPR;
        
    }

    public String getIdPR() {
        return IdPR;
    }

    public void setIdPR(String IdPR) {
        this.IdPR = IdPR;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String Deskripsi) {
        this.Deskripsi = Deskripsi;
    }

    public String getTanggalPR() {
        return TanggalPR;
    }

    public void setTanggalPR(String TanggalPR) {
        this.TanggalPR = TanggalPR;
    }

    public String getRequesterPR() {
        return RequesterPR;
    }

    public void setRequesterPR(String RequesterPR) {
        this.RequesterPR = RequesterPR;
    }

    public String getTotalPR() {
        return TotalPR;
    }

    public void setTotalPR(String TotalPR) {
        this.TotalPR = TotalPR;
    }

    
}
